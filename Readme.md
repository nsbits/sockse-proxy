# simplesocks.e

*Simple SOCKS.E* is a simple SOCKS proxy that is designed to be extendible.

It does the bare minimum of setting up the connection as described in [RFC 1982],  
and let's you add additional functionality on top as callbacks.  
Even the proxying function is implemented as a callback!

[RFC 1982]: https://www.rfc-editor.org/rfc/rfc1928

## Usage

```
./run_sockse_proxy.py --help
usage: run_sockse_proxy.py [-h] [--ip IP] [--port PORT] [--hell-o-pass]

Start SOCKS5 proxy server.

options:
  -h, --help     show this help message and exit
  --ip IP        Local IP address to start SOCKS server on (default 0.0.0.0)
  --port PORT    Local port to expose service on [1024-65535] (default 1080)
  --hell-o-pass  break client TLS hello into little chunks to resist sni snooping
```

```

./run_sockse_proxy.py --ip 127.0.0.1 --port 1080
starting SOCKS server on 127.0.0.1:1080

INFO:sockse:Client connected: ('127.0.0.1', 38282)
INFO:sockse:Client connected: ('127.0.0.1', 38298)
INFO:sockse:Client connected: ('127.0.0.1', 38314)
INFO:sockse:Client supported auth methods [0]
INFO:sockse:Client supported auth methods [0]
INFO:sockse:Client supported auth methods [0]
INFO:sockse:Received socks request SocksRequest(ver=5, cmd=1, rsv=0, atyp=1, dstaddr='9.9.9.9', dstport=443)
INFO:sockse:Received socks request SocksRequest(ver=5, cmd=1, rsv=0, atyp=1, dstaddr='9.9.9.9', dstport=443)
INFO:sockse:Received socks request SocksRequest(ver=5, cmd=1, rsv=0, atyp=1, dstaddr='9.9.9.9', dstport=443)
INFO:sockse:SocksReplyCode(rep=0, reason='Nothing unexpected, carry on')
INFO:proxy:Proxying ('127.0.0.1', 38282)<>('127.0.0.1', 1080)~('10.10.0.2', 34026)<>('9.9.9.9', 443)
INFO:sockse:SocksReplyCode(rep=0, reason='Nothing unexpected, carry on')
INFO:proxy:Proxying ('127.0.0.1', 38298)<>('127.0.0.1', 1080)~('10.10.0.2', 34042)<>('9.9.9.9', 443)
INFO:sockse:SocksReplyCode(rep=0, reason='Nothing unexpected, carry on')
INFO:proxy:Proxying ('127.0.0.1', 38314)<>('127.0.0.1', 1080)~('10.10.0.2', 34048)<>('9.9.9.9', 443)
INFO:proxy:Client probably closed connection.
INFO:proxy:Remote probably closed connection.
INFO:proxy:Closed proxy connection ('127.0.0.1', 38298)-/-/->('9.9.9.9', 443)
INFO:proxy:Client probably closed connection.
INFO:proxy:Remote probably closed connection.
INFO:proxy:Closed proxy connection ('127.0.0.1', 38314)-/-/->('9.9.9.9', 443)
```
