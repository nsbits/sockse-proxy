#!/bin/env python3

import asyncio
from sockse import SOCKS
from proxy import hell_o_pass

if __name__ == "__main__":
    # Parse inputs

    import argparse
    parser = argparse.ArgumentParser(description='Start SOCKS5 proxy server.')
    parser.add_argument('--ip', help='Local IP address to start SOCKS server on (default 0.0.0.0)',
                        default="0.0.0.0")
    parser.add_argument('--port', help="Local port to expose service on [1024-65535] (default 1080)",
                        type=int, default=1080)
    parser.add_argument('--hell-o-pass', help="break client TLS hello into little chunks to resist sni snooping",
                        action='store_true')
    args = parser.parse_args()

    # Socks up
    if args.hell_o_pass:
        # handler with plugins
        async def handle_socks_mod(crx, ctx):
            return await SOCKS.socks_handler(crx, ctx, cb_onproxystart=hell_o_pass)
        socks_handler = handle_socks_mod
        print("Enabled hell_o pass plugin")
    else:
        # regular socks handler
        socks_handler = SOCKS.socks_handler
    socks = SOCKS(args.ip, args.port)
    asyncio.run(
        socks.socksup(socks_handler=socks_handler)
    )
