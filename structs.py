from collections import namedtuple

# Structs useful throughout this library

# https://www.rfc-editor.org/rfc/rfc1928#section-3
# The values currently defined for METHOD are:
#     o  X'00' NO AUTHENTICATION REQUIRED
#     o  X'01' GSSAPI
#     o  X'02' USERNAME/PASSWORD
#     o  X'03' to X'7F' IANA ASSIGNED
#     o  X'80' to X'FE' RESERVED FOR PRIVATE METHODS
#     o  X'FF' NO ACCEPTABLE METHODS
AuthRequest = namedtuple(
    "AuthRequest",
    ["ver", "nmethods", "methods"])

SocksRequest = namedtuple(
    'SocksRequest',
    ["ver", "cmd", "rsv", "atyp", "dstaddr", "dstport"])

SocksReply = namedtuple(
    "SocksReply",
    ["ver", "rep", "rsv", "atyp", "bndaddr", "bndport"])

# https://datatracker.ietf.org/doc/html/rfc1928#section-6
# Reply field values
#      o  X'00' succeeded
#      o  X'01' general SOCKS server failure
#      o  X'02' connection not allowed by ruleset
#      o  X'03' Network unreachable
#      o  X'04' Host unreachable
#      o  X'05' Connection refused
#      o  X'06' TTL expired
#      o  X'07' Command not supported
#      o  X'08' Address type not supported
#      o  X'09' to X'FF' unassigned"""
SocksReplyCode = namedtuple(
    "SocksReplyCode",
    ["rep", "reason"])

FullAddress = namedtuple('FullAddress', ["addr", "port"])

ConnectorsCollection = namedtuple(
    'Connectors',
    ['crx', 'ctx', 'rrx', 'rtx'])

