from structs import FullAddress, ConnectorsCollection
from asyncio import open_connection
from typing import Tuple


# Proxy Commands

async def handle_cmd_connect(DST: FullAddress, 
                             Connectors: ConnectorsCollection) ->  tuple([FullAddress, ConnectorsCollection]):
    """
    Connect to destination
    Returns BND.(ADDR,PORT) and Connectors updated with remote rx,tx.

    In the reply to a CONNECT, BND.PORT contains the port number that the
    server assigned to connect to the target host, while BND.ADDR
    contains the associated IP address.  The supplied BND.ADDR is often
    different from the IP address that the client uses to reach the SOCKS
    server, since such servers are often multi-homed.  It is expected
    that the SOCKS server will use DST.ADDR and DST.PORT, and the
    client-side source address and port in evaluating the CONNECT
    request.
    """
    try:
        # Connect to remote host
        rrx, rtx = await open_connection(DST.addr, DST.port)
        bnd_addr, bnd_port = rtx.get_extra_info('sockname') [:2]
        BND = FullAddress(bnd_addr, bnd_port)
        return BND, ConnectorsCollection(Connectors.crx,Connectors.ctx,rrx,rtx)
    except Exception as e:
        raise

SOCKS_CMD_HANDLERS = {
    1: handle_cmd_connect,
}


SOCKS_CMD_NAMES = {
    # https://www.rfc-editor.org/rfc/rfc1928#section-6
    1: 'CONNECT',
    2: 'BIND',
    3: 'UDP ASSOCIATE',
}