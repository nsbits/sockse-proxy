from io import BufferedIOBase
from ipaddress import AddressValueError, IPv4Address, IPv6Address, ip_address

from structs import FullAddress, AuthRequest, SocksRequest, SocksReplyCode, SocksReply
from socks_config import VER,RSV
from socks_commands import SOCKS_CMD_HANDLERS


def parse_full_address(atyp, addr_bytes:BufferedIOBase) -> FullAddress:
    """
    Parse address values from raw bytes
    """
    match atyp:
        case 1:
            # IP V4 address (4bytes)
            addr = str(IPv4Address(addr_bytes[:4]))
            port_bytes = addr_bytes[4:]
        case 4:
            # IP V6 address (16bytes)
            addr = str(IPv6Address(addr_bytes[:16]))
            port_bytes = addr_bytes[16:]
        case 3:
            # DOMAINNAME (first byte tells size)
            size = addr_bytes[0]
            addr = addr_bytes[1:1+size].decode()
            port_bytes = addr_bytes[1+size:]
        case default:
            # unsupported atyp
            addr = None
            port_bytes = None
    if port_bytes:
        port = int.from_bytes(port_bytes[:2], byteorder='big')
    else: 
        # unread address, dont know where dstport begins
        port = None
    return (addr, port)

def atyp_full_address(full_address: FullAddress):
    """
    Recognize atyp, ie type of address (IPv4/DomainName/IPv6)
    as per https://www.rfc-editor.org/rfc/rfc1928#section-5
    Additionally None addresses are detected as atyp 0

    Raises TypeError if can't detect.
    """
    addr, port = full_address
    # No address
    if addr is None or addr == '':
        return 0
    # IP V4 address
    try: IPv4Address(addr)
    except AddressValueError: pass
    else: return 1
    # IP V6 address
    try: IPv6Address(addr)
    except AddressValueError: pass
    else: return 4
    # DOMAIN NAME address
    try: str(addr)
    except: raise TypeError
    else: return 3

def parse_socks_request(request_msg_bytes: BufferedIOBase) -> SocksRequest:
    """
    # https://datatracker.ietf.org/doc/html/rfc1928#section-4
    # +----+-----+-------+------+----------+----------+
    # |VER | CMD |  RSV  | ATYP | DST.ADDR | DST.PORT |
    # +----+-----+-------+------+----------+----------+
    # | 1  |  1  | X'00' |  1   | Variable |    2     |
    # +----+-----+-------+------+----------+----------+

    #   o  VER    protocol version: X'05'
    #   o  CMD
    #      o  CONNECT X'01'
    #      o  BIND X'02'
    #      o  UDP ASSOCIATE X'03'
    #   o  RSV    RESERVED
    #   o  ATYP   address type of following address
    #      o  IP V4 address: X'01'
    #      o  DOMAINNAME: X'03'
    #      o  IP V6 address: X'04'
    #   o  DST.ADDR       desired destination address
    #   o  DST.PORT desired destination port in network octet
    #      order"""
    ver,cmd,rsv,atyp = request_msg_bytes[:4]
    dst_bytes = request_msg_bytes[4:]
    dstaddr, dstport = parse_full_address(atyp, dst_bytes)
    socks_request = SocksRequest(ver,cmd,rsv,atyp,dstaddr,dstport)
    return socks_request

def validate_socks_request(socks_request: SocksRequest) -> SocksReplyCode:
    """
    Check sanity of parameters in socks request.

    https://datatracker.ietf.org/doc/html/rfc1928#section-6
    # Reply field values
    #      o  X'00' succeeded
    #      o  X'01' general SOCKS server failure
    #      o  X'02' connection not allowed by ruleset
    #      o  X'03' Network unreachable
    #      o  X'04' Host unreachable
    #      o  X'05' Connection refused
    #      o  X'06' TTL expired
    #      o  X'07' Command not supported
    #      o  X'08' Address type not supported
    #      o  X'09' to X'FF' unassigned"""
    # recheck version
    if socks_request.ver != VER:
        return SocksReplyCode(
            1, f'Requested version {socks_request.ver} not supported. Supported version: {VER}'
        )
    # reserved value as per standard
    if socks_request.rsv != RSV:
        return SocksReplyCode(
            2, f'Unexpected RSV: {socks_request.rsv}. Expected: {RSV}'
        )
    # # command supported?
    # if not socks_request.cmd in SOCKS_CMD_HANDLERS:
    #     return SocksReplyCode(
    #         7, f"CMD {socks_request.cmd} not supported. Supported commands: {list(SOCKS_CMD_HANDLERS)}"
    #     )
    # address present and read?
    if not socks_request.dstaddr:
        return SocksReplyCode(
            8, "DST Address expected. Received None, or couldn't parse."
        )
    # validate address
    try:
        detect_atyp = atyp_full_address(FullAddress(socks_request.dstaddr, socks_request.dstport))
    except TypeError:
        detect_atyp = 0
    if detect_atyp != socks_request.atyp:
        return SocksReplyCode(
            8, f'Failed reading address with ATYP {socks_request.atyp}. Detected ATYP: {detect_atyp}?'
        )
    # validate port
    if (isinstance(socks_request.dstport, int) and (0 <= socks_request.dstport < 2**16)):
        pass
    else: 
        return SocksReplyCode(
            8, f"invalid port number {socks_request.dstport}"
        )
    # Nothing out of the ordinary
    return SocksReplyCode(0, "Nothing unexpected, carry on")

def parse_auth_req(auth_req_msg: BufferedIOBase) -> AuthRequest:
    version, nmethods = auth_req_msg[0], auth_req_msg[1]
    if not (version == VER):
        print(f"Unimplemented SOCKS version: {version}. Best effort.")
    # Parse and process supported methods list
    methods=[]
    for i, method in zip (range(nmethods), auth_req_msg[2:]):
        methods.append(method)
    return AuthRequest(version, nmethods, methods)

def format_socks_reply(socks_reply: SocksReply) -> BufferedIOBase:
    ver, rep, rsv, atyp, bndaddr, bndport = socks_reply
    reply_msg_bytes = b''.join([
        bytes([ver,rep,rsv,atyp]),
        len(bndaddr.encode()).to_bytes(length=1, byteorder='big') if atyp==3 else b'',
        bndaddr.encode() if atyp==3 else ip_address(bndaddr).packed,
        bndport.to_bytes(length=2, byteorder='big')
    ])
    return reply_msg_bytes