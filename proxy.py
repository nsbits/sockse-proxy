#!/bin/env python3

import asyncio
from typing import Literal, Tuple, Union, Optional
from ipaddress import ip_address

import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("proxy")
logger.setLevel(logging.INFO)

EPHEMERAL_PORTS = range(32768,61000)

LocalHost = Union[Literal['localhost'], Literal['0.0.0.0'], ip_address]
Host =  Union[str, ip_address]
Port = int

async def connect(
    remotehost:Host, remoteport:Port,
    proxyhost:LocalHost, proxyport:Port,
    *args, **kwargs
    ):
    # Decorate proxyhandler with remotehost, remoteport, and **kwargs
    def decorate(proxyhandler):
        async def decorated_proxyhandler(crx, ctx):                                                                                                                                                                          
            await proxyhandler(crx, ctx, remotehost, remoteport, **kwargs)
        return decorated_proxyhandler
    regularized_proxyhandler = decorate(proxyhandler)

    # Listen for client connetion
    server = await asyncio.start_server(
        regularized_proxyhandler, proxyhost, proxyport)
    return server

async def proxyhandler(
    crx, ctx, remotehost, remoteport,
    cb_onclientconnect=None,cb_onremoteconnect=None,
    cb_onclientspeak=None, cb_onremotespeak=None,
    cb_onclientclose=None, cb_onremoteclose=None,
    cb_onproxyclose=None, **kwargs
    ):
    logger.info(f"Client connected {ctx.get_extra_info('peername')}")
    if cb_onclientconnect:
        await cb_onclientconnect(crx=crx,ctx=ctx,**kwargs)
    # Connect to remote host
    rrx, rtx = await asyncio.open_connection(remotehost, remoteport)
    logger.info(f"Remote connected {rtx.get_extra_info('peername')}")
    if cb_onremoteconnect:
        await cb_onremoteconnect(crx=crx,ctx=ctx,rrx=rrx,rtx=rtx,**kwargs)
    # Proxy
    await proxy(crx,ctx,rrx,rtx, **kwargs)
    if cb_onproxyclose:
        await cb_onproxyclose(crx, ctx, rrx, rtx,**kwargs)

async def proxy(crx, ctx, rrx, rtx, cb_onproxystart=None,
                cb_onclientspeak=None, cb_onremotespeak=None,
                cb_onclientclose=None, cb_onremoteclose=None, **kwargs):
    logger.info(
        f"Proxying"
        f" {ctx.get_extra_info('peername')}"
        f"<>{ctx.get_extra_info('sockname')}"
        f"~{rtx.get_extra_info('sockname')}"
        f"<>{rtx.get_extra_info('peername')}")
    if cb_onproxystart:
        await cb_onproxystart(crx, ctx, rrx, rtx, **kwargs)
    async def listen_client(crx, ctx, rrx, rtx):
        logger.debug("Waiting to hear from client")
        try:
            clientdata = await crx.read(64* 2**10)
            assert(clientdata != b'')
        except Exception as e:
            logger.debug(str(e))
            logger.debug(f"Recieved empty data from client or above error")
            logger.debug(f"clientdata: {clientdata}")
            logger.info("Client probably closed connection.")
            if cb_onclientclose:
                await cb_onclientclose(crx=crx,ctx=ctx,rrx=rrx,rtx=rtx,**kwargs)
            logger.debug("Closing remote tx since client won't talk")
            rtx.close()
            await rtx.wait_closed()
        else:
            logger.debug("Recieved data from client")
            logger.debug(f"clientdata: {clientdata}")
            if cb_onclientspeak:
                await cb_onclientspeak(
                    crx=crx,ctx=ctx,rrx=rrx,rtx=rtx,
                    clientdata=clientdata,**kwargs)
            logger.debug("Passing forward")
            rtx.write(clientdata)
            await rtx.drain()
            logger.debug("Sent clientdata to remote.")
            # Repeat
            asyncio.create_task(listen_client(crx, ctx, rrx, rtx))
    async def listen_remote(crx, ctx, rrx, rtx):
        logger.debug("Waiting for remote's move")
        try:
            remotedata = await rrx.read(64* 2**10)
            assert (remotedata != b'')
        except Exception as e:
            logger.debug(str(e))
            logger.debug("Recieved empty data from remote or above error")
            logger.debug(f"remotedata: {remotedata}")
            logger.info("Remote probably closed connection.")
            if cb_onremoteclose:
                await cb_onremoteclose(crx=crx,ctx=ctx,rrx=rrx,rtx=rtx,**kwargs)
            logger.debug("Closing client tx since server done talking")
            ctx.close()
            await ctx.wait_closed()
        else:
            logger.debug(f"Recieved data from remote")
            logger.debug(f"remotedata: {remotedata}")
            if cb_onremotespeak:
                await cb_onremotespeak(
                    crx=crx,ctx=ctx,rrx=rrx,rtx=rtx,
                    remotedata=remotedata,**kwargs)
            logger.debug("Passing back")
            ctx.write(remotedata)
            await ctx.drain()
            logger.debug("Sent remotedata back to client.")
            # Repeat
            asyncio.create_task(listen_remote(crx, ctx, rrx, rtx))
    asyncio.create_task(listen_client(crx, ctx, rrx, rtx))
    asyncio.create_task(listen_remote(crx, ctx, rrx, rtx))
    await ctx.wait_closed()
    await rtx.wait_closed()
    logger.info(
        "Closed proxy connection {}-/-/->{} ". format(
            ctx.get_extra_info('peername'),
            rtx.get_extra_info('peername'))
    )


async def hell_o_pass(crx, ctx, rrx, rtx, MTU=160):
    "Evade SNI DPI snooping by breaking ClientHello into smaller chunks"
    "Should be the first thing to do after establishing tunnels on both sides"
    clienthello = await crx.read(64* 2**10)
    # assuming clienthello is smaller in size than 64KiB
    logger = logging.getLogger("hell-o-pass")
    logger.info(f"ClientHello Size: {len(clienthello)}")
    logger.debug(f"ClientHello Bytes: {clienthello}")
    # pass it forward in MTU sized byte chunks
    from io import BytesIO
    clienthellostream = BytesIO(clienthello)
    logger.info(f"Chunking ClientHello into {MTU}byte sized packets")
    while chunk := clienthellostream.read(MTU):
        rtx.write(chunk)
        await rtx.drain()
        # before moving to next chunk
        # wait a bit for the system to flush network buffers
        await asyncio.sleep(1/2**16) # ~7.63ms
    
async def connect_autoport(
    remotehost:Host, remoteport:Port,
    proxyhost:LocalHost='localhost', proxyport=None, max_retry=10,
    *args, **kwargs,
    ):
    if proxyport:
        try:
            logger.info(f"Starting proxy server on {proxyhost}:{proxyport}"
                        f" for remote {remotehost}:{remoteport}")
            # print(f"Proxy for {remotehost}{remoteport}"
            #       f" listening on {proxyhost}:{proxyport}")
            server = await connect(
                remotehost=remotehost, remoteport=remoteport, 
                proxyhost=proxyhost, proxyport=proxyport,
                **kwargs)
        except OSError as oserr:
            if oserr.errno==98: # port in use
                logger.warning(oserr.strerror)
                pass
    import random
    for t in range (max_retry):
        proxyport = random.randrange(EPHEMERAL_PORTS.start, EPHEMERAL_PORTS.stop)
        try:
            logger.info(f"Starting proxy server on {proxyhost}:{proxyport}"
                        f" for remote {remotehost}:{remoteport}")
            server = await connect(
                remotehost=remotehost, remoteport=remoteport, 
                proxyhost=proxyhost, proxyport=proxyport,
                **kwargs)
        except OSError as oserr:
            if oserr.errno==98: # port in use
                logger.warning(oserr.strerror)
                # print(oserr.strerror)
                continue # to next try
        else: break
    return server

if __name__=='__main__':

    import argparse
    parser = argparse.ArgumentParser('Start a proxy')
    parser.add_argument('remotehost', type=str, help="Remote hostname or IP address to connect to")
    parser.add_argument('remoteport', type=int, help="Remote port to connect at")
    parser.add_argument('--proxyhost', type=str, help="Hostname or IP address of this (proxying) computer to listen on for clients",
                        default='0.0.0.0')
    parser.add_argument('--proxyport', type=int, help="Preffered port number at proxyhost address to listen for clients")
    parser.add_argument('--mtu', type=int, help="Size of chunk in bytes to break TLS ClientHello into for SNI DPI evasion ",
                        default=64)
    args = parser.parse_args()

    async def main():
        server = await connect_autoport(
            remotehost=args.remotehost, remoteport=args.remoteport,
            proxyhost=args.proxyhost, proxyport=args.proxyport,
            cb_onremoteconnect=hell_o_pass,MTU=args.mtu)

        socket = server.sockets[0]
        listeninghost,listeningport = socket.getsockname()
        print(f"Proxy for {args.remotehost}:{args.remoteport}"
              f" listening at {listeninghost}:{listeningport}")

        async with server:
            await server.serve_forever()

    asyncio.run(main())

