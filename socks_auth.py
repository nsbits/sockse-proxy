# Authentication methods

async def handle_method_0(rx,tx) -> bool:
    """
    No Authentication Required

    return: True if auth sub-negotiations succeeded
            False if auth sub-negotiations failed
    """
    return True

async def handle_method_255(rx,tx) -> bool:
    """
    No Acceptable Methods

    return: True if auth sub-negotiations succeeded
            False if auth sub-negotiations failed
    """
    return False

AUTH_METHOD_HANDLERS = {
    0:  handle_method_0,  # 'No Authentication Required',
    #2: handle_method_2,  # 'Username/Password', #(not yet implemented)
    255:handle_method_255,#'No Acceptable Methods'
}


AUTH_METHOD_NAMES = {
    # https://www.iana.org/assignments/socks-methods/socks-methods.xhtml
    0 : 'No Authentication Required',                   #[RFC1928]
    1 : 'GSSAPI',                                       #[RFC1928]
    2 : 'Username/Password', 	                        #[RFC1929]
    3 : 'Challenge-Handshake Authentication Protocol',  #[Marc_VanHeyningen]
    4 : 'Unassigned',
    5 : 'Challenge-Response Authentication Method',     #[Marc_VanHeyningen]
    6 : 'Secure Sockets Layer', 	                    #[Marc_VanHeyningen]
    7 : 'NDS Authentication', 	                        #[Vijay_Talati]
    8 : 'Multi-Authentication Framework',               #[Dan_Fritch]
    9 : 'JSON Parameter Block',                         #[Brandon_Wiley]
    255 : 'No Acceptable Methods',                      #[RFC1928]
}
AUTH_METHOD_NAMES.update(
    {i: 'Unassigned' for i in range(10,128)}            #[RFC1928] 
)
AUTH_METHOD_NAMES.update(
    {i: 'Reserved for Private Methods' for i in range(128,256)} #[RFC1928]
)