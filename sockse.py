import asyncio
import logging
from collections import namedtuple
from io import BufferedIOBase

from socks_config import RSV, VER
from socks_helpers import (
    atyp_full_address,
    format_socks_reply,
    parse_auth_req,
    parse_socks_request,
    validate_socks_request,
)
from proxy import proxy
from socks_auth import AUTH_METHOD_HANDLERS
from socks_commands import SOCKS_CMD_HANDLERS
from structs import (
    ConnectorsCollection,
    FullAddress,
    SocksReply,
    SocksReplyCode,
    SocksRequest,
)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("sockse")
logger.setLevel(logging.INFO)


class SOCKS:
    """
    Container for socks config and controls
    """

    def __init__(self, socks_ip: str, socks_port: int = 1080, socks_handler=None):
        self.socks_addr = socks_ip
        self.socks_port = socks_port
        if socks_handler:
            self.socks_handler = self.socks_handler

    async def socksup(self, socks_handler=None):
        if not socks_handler:
            socks_handler = self.socks_handler
        # start listening on socks_ip:socks_port for connections
        print(f"starting SOCKS server on {self.socks_addr}:{self.socks_port}")
        server = await asyncio.start_server(
            socks_handler, self.socks_addr, self.socks_port
        )
        async with server:
            await server.serve_forever()

    @staticmethod
    async def socks_handler(crx, ctx, **kwargs):
        client = ctx.get_extra_info("peername")
        logger.info(f"Client connected: {client}")
        "Handle flow of SOCKS connection: version, auth, requests"
        # Version identification/auth method selection
        # 1b (ver) + 1b (nmethods) + upto 255b (methods)
        auth_req_msg = await crx.read(257)
        if not auth_req_msg:
            ctx.close()
            await ctx.wait_closed()
            return
        auth_request = parse_auth_req(auth_req_msg)
        selected_method = None
        logger.info(f"Client supported auth methods {auth_request.methods}")
        for method in auth_request.methods:
            if method in AUTH_METHOD_HANDLERS:
                selected_method = method
        if selected_method is None:
            selected_method = 255  # No Acceptable Methods
        auth_selection_msg = bytes([VER, selected_method])
        ctx.write(auth_selection_msg)
        await ctx.drain()

        # Handle auth sub-negotiations
        auth_handler = AUTH_METHOD_HANDLERS.get(selected_method, 255)
        authenticated = await auth_handler(crx, ctx)
        if authenticated == False:
            ctx.close()
            await ctx.wait_closed()
            return  # nothing more to do, exit

        # Handle SOCKS request
        # |VER | CMD |  RSV  | ATYP | DST.ADDR | DST.PORT |
        # +----+-----+-------+------+----------+----------+
        # | 1  |  1  | X'00' |  1   | Variable |    2     |
        #   1b +  1b +  1b   +  1b  + (1+255b) +   2b
        socks_req_msg = await crx.read(262)
        logger.debug(f"Evaluating socks request {socks_req_msg}")
        if not socks_req_msg:
            ctx.close()
            await ctx.wait_closed()
            return
        socks_request = parse_socks_request(socks_req_msg)
        logger.info(f"Received socks request {socks_request}")
        socks_req_status = validate_socks_request(socks_request)
        error, details = socks_req_status
        if error:
            pass
        elif socks_request.cmd not in SOCKS_CMD_HANDLERS:
            socks_req_status = SocksReplyCode(
                7,
                f"Command {socks_request.cmd} not supported. "
                f"Supported commands: {list(SOCKS_CMD_HANDLERS)}",
            )
        else:
            try:
                cmd_handler = SOCKS_CMD_HANDLERS.get(socks_request.cmd, 1)
                Connectors = ConnectorsCollection(crx, ctx, None, None)
                dst = FullAddress(socks_request.dstaddr, socks_request.dstport)
                bnd, Connectors = await cmd_handler(dst, Connectors)
                bnd_atyp = atyp_full_address(bnd)
            except ConnectionRefusedError as e:
                socks_req_status = SocksReplyCode(5, f"Connection refused {e}")
                bnd, bnd_atyp = FullAddress("", 0), 3
            except TimeoutError as e:
                socks_req_status = SocksReplyCode(6, f"Timeout error {e}")
                bnd, bnd_atyp = FullAddress("", 0), 3
            except OSError as e:
                if e.errno == 113:
                    socks_req_status = SocksReplyCode(4, f"Host unreachable {e}")
                else:
                    socks_req_status = SocksReplyCode(1, repr(e))
                bnd, bnd_atyp = FullAddress("", 0), 3
            except Exception as e:
                socks_req_status = SocksReplyCode(1, repr(e))
                bnd, bnd_atyp = FullAddress("", 0), 3
        logger.info(f"{socks_req_status}")
        rep, details = socks_req_status
        socks_reply = SocksReply(VER, rep, RSV, bnd_atyp, bnd.addr, bnd.port)
        socks_rep_msg = format_socks_reply(socks_reply)
        ctx.write(socks_rep_msg)
        await ctx.drain()

        # Pass on to proxy if everything is alright
        if socks_reply.rep == 0:
            # all is well
            await proxy(*Connectors, **kwargs)

        # Close connection
        ctx.close()
        await ctx.wait_closed()

    _socks_handler = socks_handler
